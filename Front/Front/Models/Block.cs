﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Front.Models
{
    public class Block
    {
        public string Message { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public byte[] PreviousBlockHash { get; set; }
    }
}