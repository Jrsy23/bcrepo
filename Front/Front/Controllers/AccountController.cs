﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Web.Security;
namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Registration()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Registration(RegInModel model)
        {
            if (ModelState.IsValid )
            {

                if (User == null)
                {
                    
                    ///добавляем нового пользователя

                    if (User != null) // если добавлен успешно
                        {
                        FormsAuthentication.SetAuthCookie(model.name, true);
                             return RedirectToAction("Index", "Home");
                       }
                }
                else
                {

                    ModelState.AddModelError("", "Пользователь с таким ИНН уже зарегистрирован");
                }
            }

            return View(model);

        }
        
        public ActionResult LogIn()
        {
            return View();
        }


        [HttpPost]
        public ActionResult LogIn (LogInModel model)
        {
            if (ModelState.IsValid)
            {

                if (User == null)
                {

                    ///добавляем нового пользователя

                    if (User != null) // если добавлен успешно
                    {
                        //FormsAuthentication.SetAuthCookie(model.name, true);
                        //return RedirectToAction("Index", "Home");
                    }
                }
                else
                {

                    ModelState.AddModelError("", "Пользователь с таким ИНН и паролем не существует");
                }
            }

            return View(model);

        }



    }
}